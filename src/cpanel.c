/* 
 * Linux Labs - http://linuxlabs.lci.ufrj.br
 * GNOME Configurator Panel
 * Authors: Diego Delgado Lages (lages@linuxlabs.lci.ufrj.br)
 *          Andr� Duarte de Souza (luke@linuxlabs.lci.ufrj.br)
 */ 

#include <gnome.h>
#include <gdk_imlib.h>
#include <gtk-xmhtml/gtk-xmhtml.h>

#include "plugin.h"

int oldselectedicon=0;

GtkWidget *html;
GtkWidget *clist;

int  toggle=0;
int  r_printer=0;
int  r_dev=0;
int  r_net=0;
int  r_par=0;
int  r_mod=0;
int  r_user=0;

struct item *search(int number) {
struct item *x;
x = head;
while (x->number != number) x = x->next;
return x;
}		  

void Select_Icon(GtkWidget *iconlist,gint num, gint acolumn,
		 			 GdkEventButton *event, gpointer data)
{
struct item *pt;
int i;
char joined[1024];
char join1[]="<BODY BACKGROUND=\"/usr/local/share/images/bg.jpg\">\0";
char join2[]="<img src=\"/usr/local/share/icons/\0";
// join3 == pt->icon
char join4[]="\"><font face=\"Arial,Times\" size=+2><b>\0";
// join5 == pt->name
char join6[]="<b></font>\n<br><br><br>\n<font face=\"Arial,Times\" size=-2>\0";
// join7 == pt->text
char join8[]="</font>\n</BODY>\0";
		  
  	if (num == oldselectedicon)
  	{
		if (toggle)
		{

		   pt = search(num+1);
		   system(pt->exec);	
				  
			printf("Call...\n");

			toggle=0;

			return;
		}
		else
		{
			toggle++;
			return;
		}

	}

	toggle=0;

pt = NULL;
pt = search(num+1);

strcpy(joined,join1);
strcat(joined,join2);
strcat(joined,pt->icon);
strcat(joined,join4);
strcat(joined,pt->name);
strcat(joined,join6);
strcat(joined,pt->text);
strcat(joined,join8);

gtk_xmhtml_source(GTK_XMHTML(html),joined);
					 
	oldselectedicon=num;

}


GdkImlibImage *load_icon(char *s) {
char load[120]="/usr/local/share/icons/";
strcat(load,s);
return gdk_imlib_load_image(load);
}

/* Main Funtion. */
int main(int argc, char *argv[])
{
	GnomeApp  *window;
	GtkWidget *button;
	GtkWidget *box1;
	GtkWidget *scroll_win;
	GdkImlibImage *pix;
	struct item *x;
	int i,z;

	init_plugin();
	
	oldselectedicon=itens+1;
	
	gnome_init("The Configurator Panel","0.3.7",argc,argv);

   window = gnome_app_new("The Configurator Panel","The Configurator Panel"); 
	
	gtk_widget_realize(window);

	gtk_widget_set_usize(window,500,200);

	box1= gtk_hbox_new (FALSE, 0);

	gnome_app_set_contents (GNOME_APP(window),box1);

	gtk_container_add(GTK_CONTAINER(window), box1);

   scroll_win = gtk_scrolled_window_new (NULL,NULL);

	gtk_scrolled_window_set_policy ( GTK_SCROLLED_WINDOW(scroll_win),
												GTK_POLICY_AUTOMATIC,
												GTK_POLICY_AUTOMATIC); 
	html = gtk_xmhtml_new();

	gtk_xmhtml_source(GTK_XMHTML(html),"<BODY BACKGROUND=\"/usr/local/share/images/bg.jpg\">The Configurator Panel</BODY>");

	gtk_box_pack_start(GTK_BOX(box1),html, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(box1), scroll_win, TRUE, TRUE, 0);    

	gtk_widget_set_usize(html,230,-1);
	
	clist = gnome_icon_list_new (80,NULL, TRUE);

	gtk_container_add(GTK_CONTAINER(scroll_win),clist);

 x = head;
 for (z=1;z<itens+1;z++) {
   x = x->next;
   pix = load_icon(x->icon); 
	if (pix) {
	gdk_imlib_render (pix, pix->rgb_width,pix->rgb_height);
	gnome_icon_list_append_imlib(GNOME_ICON_LIST(clist), pix, x->name);
} 
}

	gtk_signal_connect(GTK_OBJECT(window),"destroy",
							 GTK_SIGNAL_FUNC(gtk_main_quit),NULL);
	
	gtk_signal_connect(GTK_OBJECT(clist), "select_icon",
							 GTK_SIGNAL_FUNC(Select_Icon),NULL);

	gtk_widget_show(html);

	gtk_widget_show(clist);

	gtk_widget_show(scroll_win);

	gtk_widget_show(box1);

	gtk_widget_show(window);

	gtk_main();
   clear();
	return 0;
}
