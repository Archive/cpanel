/*Extracted form mtext library */
char *cut_word(char *word, int start, int len)
{
    char *buffer = NULL;
    int tmp1 = 0, tmp2 = 0;


    buffer = (char *) malloc(len + 1);

    for (tmp1 = start; tmp1 < start + len; tmp1++) {
	buffer[tmp2] = word[tmp1];
	tmp2++;
    }

    buffer[tmp2] = '\0';
    return buffer;
}



/*Extracted from mtext library */
char *extract_word(char *string, int num, char *delim)
{
    char *word[1024];
    static char buffer[1024];
    int i = 0;

    strncpy(buffer, string, 1024);
    (char *) word[i] = (char *) strtok(buffer, delim);
    while (((char *) word[++i] = (char *) strtok(NULL, delim)));
    word[i] = NULL;

    if (word[num] == NULL)	/* Catch this rather than sig11'ing */
	return NULL;

    if (strstr(word[num], "\n") != NULL)
	return cut_word(word[num], 0, strlen(word[num]) - 1);
    else
	return (word[num]);
}

int *find_bword(char *word, char *file)
{
    FILE *fic;
    char *buffer;

    buffer = (char *) malloc(1024);

    if ((fic = fopen(file, "r")) == NULL) {
	fprintf(stderr, "Couldn't open file %s..\n", file);
	return NULL;
    }
    while (fgets(buffer, 500, fic) != NULL) {
	if (strncmp(buffer, word,strlen(word)) == 0 ) 
	{
	    fclose(fic);
		 return 1;
	}
    }
    fclose(fic);

    return 0;
}



/*Extracted from mtext library */
char *find_word(char *word, char *file)
{
    FILE *fic;
    char *buffer;

    buffer = (char *) malloc(1024);

    if ((fic = fopen(file, "r")) == NULL) {
	fprintf(stderr, "Couldn't open file %s..\n", file);
	return NULL;
    }
    while (fgets(buffer, 500, fic) != NULL) {
	if (strstr(buffer, word) != NULL) {
	    fclose(fic);

	    if (strstr(word, "\n") != NULL)
		return cut_word(buffer, 0, strlen(buffer) - 1);
	    else
		return buffer;
	}
    }
    fclose(fic);

    return NULL;
}
