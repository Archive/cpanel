/*  CUser
 *  Copyright (C) 1999 Andre Duarte de Souza
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <crypt.h>
#include <time.h>

//#include "gladesrc.h"
//#include "user.h"

  GtkWidget *princ;
  GtkWidget *window2;
  GtkWidget *window3;
  GtkWidget *window4;
//  GtkWidget *msenha; 
  GtkWidget *existe; 
  
  GtkWidget *entry_log;

  GtkWidget *sentry1;
  GtkWidget *sentry2;
  
  GtkWidget *entry2;
  GtkWidget *entry3;
  GtkWidget *entry4;
  GtkWidget *entry5;
  GtkWidget *entry6;
  GtkWidget *checkbutton1;
  GtkWidget *Editar; 
  GtkWidget *clist1; 
 
  char edit_flag=0;
  char criando = 0;
  char first_time = 1;
  
  gchar *nome,*endereco,*login,*tel1,*tel2,*coment;

//#include "guser_s.h"

char comp1[20] = ":x:yyyy:yyyy::/home/";
char virgula=',';
char comp2[11] = ":/bin/bash\n";
int uid;
FILE *passwd;

void encontra(int);

void deline(int pos) {
char buffer[16384];
int i=0,k=0;
passwd = fopen("/etc/passwd","r+b");
if (passwd) {
while(!feof(passwd)) {fread(&buffer[i],1,1,passwd);i++;}
fclose(passwd);
}
rename("/etc/passwd","/etc/bak.passwd");
passwd = fopen("/etc/passwd","wb");
if (passwd) {
fwrite(&buffer[0],pos,1,passwd);
while (buffer[pos+k] != 10 && buffer[pos+k] != EOF) k++;
k++;
fwrite(&buffer[pos+k],(i-1-pos-k),1,passwd);
fclose(passwd);
}
}	  


// A funcao abaixo encontra o maior uid disponivel para usuario comum
void encontra(int d){
int maior=0,temp;
char byte,numero[5],count=0,x[5],i;
uid = 0;

if (d==-1) {

fseek(passwd,0,SEEK_SET);

for (i=0;i<5;i++) numero[i] = '0';

maior=999;

while (!feof(passwd)) {
fread(&byte,1,1,passwd);
if (byte==':') { 

  if (count<2 || (count>2 && count<6)) count++;	  

  if (count==6) count = 0; else

  if (count==2) {
  count++;
  for (i=0;i<5;i++) numero[i] = '0'; 
  i=0; 
  do {fread(&x[i],1,1,passwd);i++;} while (x[i-1]!=':' && x[0]!=6); i--;
  if (i==1) numero[3] = x[0]; else
  if (i==2) {numero[2] = x[0]; numero[3] = x[1];} else
  if (i==3) {numero[1] = x[0];numero[2] = x[1];numero[3] = x[2];} else
  if (i==4) {numero[0] = x[0];numero[1] = x[1];numero[2] = x[2];numero[3] = x[3];}
//  printf("Numero - %s\n",numero);
  temp = (numero[0] - '0')*1000 + (numero[1] - '0')*100 +(numero[2] - '0')*10 +(numero[3] - '0');
//  printf("Temp : %d\n\n",temp);
  if (temp>maior && temp < 62000 && temp > 1000) maior = temp;
		  	 
  } //if (count == 2)
} //if (byte == ':')
} //while
maior++;
//printf("MAIOR = %d\n\n",maior-1);
uid = maior;
}
else maior=d;
numero[0] = (maior/1000) + '0';
numero[1] = ((maior/100)%10) + '0';
numero[2] = ((maior/10)%100) + '0';
numero[3] = (maior%10) + '0';
comp1[3] = numero[0];
comp1[4] = numero[1];
comp1[5] = numero[2];
comp1[6] = numero[3];
comp1[8] = numero[0];
comp1[9] = numero[1];
comp1[10] = numero[2];
comp1[11] = numero[3];
} //encontra 	  

//
//#include "senha.h"

char car[3];
char *global = "123456789ABCD";

void gera_dcar() {
int j;
char t;
for (t=0;t<2;t++) {
srand(time(0)*(t+1));
j = (int)(61.0*rand()/(RAND_MAX+1.0));
// a to z, A to Z, 0 to 9, plus . and /
if (j<12) car[t] = (char)(j+46); else
if (j>11 && j<37) car[t]=(char)(j+54); else //65 - 11 = 54
if (j>36) car[t]=(char)(j+61); // 97 - 36 = 61
}
car[2]=0;
//j=car[0];k=car[1];
//printf("\nTestados = %d e %d\n",j,k);
//printf("Caracteres : %s\n",car);
//return car;
}

void gera_senha(char *normal) {
gera_dcar();
global = crypt(normal,car);
//printf("\nGlobal : %s\n",global);
//return global;
}	


//
FILE *shadow;

int find_s() {
char buffer[2048];
int i=0,k,user=1,j=0;
shadow = fopen("/etc/shadow","rb");
if (shadow) {
while (!feof(shadow)) {
user = 1;
j += i;
i=0;
//pega linha
while	(buffer[i-1] != 10 && !feof(shadow))  {fread(&buffer[i],1,1,shadow);i++;}
//procura login
for (k=0;k<strlen(login);k++) 
  if (buffer[k] != login[k]) 
  {user = 0;break;}
if (user && buffer[strlen(login)] == ':') {fclose(shadow);return j;}
} //while !feof
fclose(shadow);	  
return -1;
}	  
return -2;
}	  

void deline_s(int pos) {
char buffer[16384];
char fim = 0;
int i=0,k=0;
if (pos != -1) {
shadow = fopen("/etc/shadow","r+b");
if (shadow) {
while(!feof(shadow)) {fread(&buffer[i],1,1,shadow);i++;}
fclose(shadow);
}
rename("/etc/shadow","/etc/bak.shadow");
shadow = fopen("/etc/shadow","wb");
if (shadow) {
fwrite(&buffer[0],pos,1,shadow);
while (buffer[pos+k] != 10 && buffer[pos+k] != buffer[i-1]) 
{ k++; if (buffer[pos+k] == buffer[i-1]) fim++;}
k++;
if (!fim) fwrite(&buffer[pos+k],(i-1-pos-k),1,shadow);
fclose(shadow);
}
}
}	  

void grava_s(int muda,char *senha) {
char data[19] = ":*:0:0:99999:7:::\n\0";
char i=0;
shadow = fopen("/etc/shadow","a+b");
fwrite(&login[0],strlen(login),1,shadow);
if (muda) {
fwrite(&data[0],1,1,shadow);
gera_senha(senha);
//printf("\nGravando : %s , Tamanho : %d\n",global,strlen(global));
fwrite(&global[0],strlen(global),1,shadow);
i=2;
} 
fwrite(&data[i],strlen(data)-i,1,shadow);
fclose(shadow);
}	


//
int alocou=0,alocou_l=0;
char home[30];

void r_remove(GtkWidget *, gpointer);
GtkWidget *create_Remove(void);
GtkWidget *create_window2(void);
void create_senha(void);

GtkWidget*
get_widget                             (GtkWidget       *widget,
                                        gchar           *widget_name)
{
  GtkWidget *found_widget;

  if (widget->parent)
    widget = gtk_widget_get_toplevel (widget);
  found_widget = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (widget),
                                                   widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}

/* This is an internally used function to set notebook tab widgets. */
void
set_notebook_tab                       (GtkWidget       *notebook,
                                        gint             page_num,
                                        GtkWidget       *widget)
{
  GtkNotebookPage *page;
  GtkWidget *notebook_page;

  page = (GtkNotebookPage*) g_list_nth (GTK_NOTEBOOK (notebook)->children, page_num)->data;
  notebook_page = page->child;
  gtk_widget_ref (notebook_page);
  gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), page_num);
  gtk_notebook_insert_page (GTK_NOTEBOOK (notebook), notebook_page,
                            widget, page_num);
  gtk_widget_unref (notebook_page);
}


void mensagem(char *s) {
  GtkWidget *aviso;
  GtkWidget *fixed2;
  GtkWidget *button2;
  GtkWidget *label9;

  aviso = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (aviso), "aviso", aviso);
  gtk_widget_set_usize (aviso, 250, 60);
  gtk_window_set_title (GTK_WINDOW (aviso), "Warning");
  gtk_window_set_policy (GTK_WINDOW (aviso), TRUE, TRUE, FALSE);

  fixed2 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (aviso), "fixed2", fixed2);
  gtk_widget_show (fixed2);
  gtk_container_add (GTK_CONTAINER (aviso), fixed2);

  button2 = gtk_button_new_with_label ("OK");
  gtk_object_set_data (GTK_OBJECT (aviso), "button2", button2);
  gtk_widget_show (button2);
  gtk_fixed_put (GTK_FIXED (fixed2), button2, 85, 30);
  gtk_widget_set_usize (button2, 76, 25);
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (aviso));

//"Este usuario n�o pode ser editado   " 
  label9 = gtk_label_new (s);
  gtk_object_set_data (GTK_OBJECT (aviso), "label9", label9);
  gtk_widget_show (label9);
  gtk_fixed_put (GTK_FIXED (fixed2), label9, 8, 5);
  gtk_widget_set_usize (label9, 250, 20);
  gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_CENTER);

  gtk_widget_show(aviso);
}	  


void tiradp(char *s) {
int i;
for (i=0; i<strlen(s); i++)
 if (s[i] == ':') s[i] = 32;
}	  

void ajeita(char *s) {
int i;	  
for (i=0; i<strlen(s); i++)
 if (s[i] == 32) s[i] = '_';	  
}	  

void fmem() {
if (alocou) {
//g_free(login);
g_free(nome);
g_free(endereco);
g_free(tel1);
g_free(tel2);
g_free(coment);
}
}	  

void homedir() {
char dest[30] = "/home/\0xxxxxxxxxxxxxxxxxxxxxx";
strcat(dest,login);	  
strcpy(home,dest);
}	  


void mkhome() {
homedir();
//printf("\nCriando :");
//printf("\n%s\n",&home[0]);
if (mkdir(home,S_IRWXU| S_IROTH | S_IRGRP | S_IXOTH | S_IXGRP)) mensagem("\nError creating home directory\n");
if (chown(home,uid,-1)) printf("\nError changing owner\n");
}

void fmeml() {
if (alocou_l) {g_free(login);alocou_l=0;}
}	  
	  
void ler_login() {
//if (alocou_l) fmeml();
login = gtk_editable_get_chars(GTK_EDITABLE(entry_log),0,-1);	  
if (strlen(login) > 0) {tiradp(login); ajeita(login);}
alocou_l = 1;
}	  


 

void ler_entrys() {
//if (alocou) fmem();
//login = gtk_editable_get_chars(GTK_EDITABLE(entry1),0,-1);
nome = gtk_editable_get_chars(GTK_EDITABLE(entry2),0,-1); 
endereco = gtk_editable_get_chars(GTK_EDITABLE(entry3),0,-1); 
tel1 = gtk_editable_get_chars(GTK_EDITABLE(entry4),0,-1); 
tel2 = gtk_editable_get_chars(GTK_EDITABLE(entry5),0,-1); 
coment = gtk_editable_get_chars(GTK_EDITABLE(entry6),0,-1); 

if (strlen(nome) > 0) tiradp(nome);
if (strlen(endereco) > 0) tiradp(endereco);
if (strlen(tel1) > 0) tiradp(tel1);
if (strlen(tel2) > 0) tiradp(tel2);
if (strlen(coment) > 0) tiradp(coment);

alocou = 1;
}	  

int find() {
char buffer[2048];
int i=0,k,user=1,j=0;
passwd = fopen("/etc/passwd","rb");
if (passwd) {
while (!feof(passwd)) {
user = 1;
j += i;
i=0;
//pega linha
while	(buffer[i-1] != 10 && !feof(passwd))  {fread(&buffer[i],1,1,passwd);i++;}
//procura login
for (k=0;k<strlen(login);k++) 
  if (buffer[k] != login[k]) 
  {user = 0;break;}
if (user && buffer[strlen(login)] == ':') {fclose(passwd);return j;}
} //while !feof
fclose(passwd);	  
return -1;
}	  
return -2;
}	  

int pega_uid() {
int i,ret=-1;
char byte,numero[6];
i = find();
passwd = fopen("/etc/passwd","rb");
if (passwd) {
fseek(passwd,i,SEEK_SET);
while (byte != ':') fread(&byte,1,1,passwd);
byte=0;
while (byte != ':') fread(&byte,1,1,passwd);
for (i=0;i<6;i++) numero[i] = 0;
i=-1;
do {i++;fread(&numero[i],1,1,passwd);} while (numero[i] != ':'); 
if (i==1) ret = numero[0]-'0'; 
if (i==2) ret = (numero[0]-'0')*10 + numero[1]-'0'; 
if (i==3) ret = (numero[0]-'0')*100 + (numero[1]-'0')*10 + numero[2]-'0'; 
if (i==4) ret = (numero[0]-'0')*1000 + (numero[1]-'0')*100 + (numero[2]-'0')*10 + numero[3]-'0';
if (i==5) ret = (numero[0]-'0')*10000 + (numero[1]-'0')*1000 + (numero[2]-'0')*100 + (numero[3]-'0')*10 + numero[4]-'0';  
fclose(passwd);
}
return ret;
}

void get_login(GtkWidget *x, gpointer y) {
ler_login();	  
if (strlen(login) > 0 && strlen(login) < 25 && find() == -1) {
gtk_widget_destroy(window3);
/*
if (find() != -1) {
j = pega_uid();
if (j>=1000 && j<63000) {
Editar = create_editar();
gtk_widget_show(Editar);
return;} 
*/
window2 = create_window2();
gtk_widget_show (window2);
} // if strlen
else mensagem("This user cannot be created.  ");
//printf("\n%s\n",login);
}	  

void c_criando(GtkWidget *x, gpointer y) {
if (criando) criando = 0;
gtk_widget_destroy(x);
}

GtkWidget*
create_login_win ()
{
  GtkWidget *login_win;
  GtkWidget *fixed1;
  GtkWidget *label1;
  GtkWidget *button1;
  GtkWidget *button2;

   login_win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (login_win), "login_win", login_win);
  gtk_widget_set_usize (login_win, 410, 70);
  gtk_window_set_title (GTK_WINDOW (login_win), "Login");
  gtk_window_set_policy (GTK_WINDOW (login_win), TRUE, TRUE, FALSE);

  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (login_win), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (login_win), fixed1);

  label1 = gtk_label_new ("Login");
  gtk_object_set_data (GTK_OBJECT (login_win), "label1", label1);
  gtk_widget_show (label1);
  gtk_fixed_put (GTK_FIXED (fixed1), label1, 0, 5);
  gtk_widget_set_usize (label1, 112, 16);

  entry_log = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (login_win), "entry_log", entry_log);
  gtk_widget_show (entry_log);
  gtk_fixed_put (GTK_FIXED (fixed1), entry_log, 110, 5);
  gtk_widget_set_usize (entry_log, 200, 21);

  button1 = gtk_button_new_with_label ("OK");
  gtk_object_set_data (GTK_OBJECT (login_win), "button1", button1);
  gtk_widget_show (button1);
  gtk_fixed_put (GTK_FIXED (fixed1), button1, 125, 40);
  gtk_widget_set_usize (button1, 80, 21);
  gtk_signal_connect_object (GTK_OBJECT (button1), "clicked",
                             GTK_SIGNAL_FUNC (get_login),
                             GTK_OBJECT(button1));
/*
  gtk_signal_connect_object (GTK_OBJECT (button1), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (login_win));
*/
  button2 = gtk_button_new_with_label ("Cancel");
  gtk_object_set_data (GTK_OBJECT (login_win), "button2", button2);
  gtk_widget_show (button2);
  gtk_fixed_put (GTK_FIXED (fixed1), button2, 225, 40);
  gtk_widget_set_usize (button2, 80, 21);
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_show),
                             GTK_OBJECT (princ));
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (c_criando),
                             GTK_OBJECT (login_win));

  return login_win;
}

void r_login_win(GtkWidget *x, gpointer y) {
criando = 1;
window3 = create_login_win();
gtk_widget_hide (princ);
gtk_widget_show (window3);
}	 

/////////////////////////////////////////////////////////////

void lista_login() {
char byte[70],pos,x;	  
int quantos=0,loop,id,onde;
gchar *primeiro,*old;
char peguei=0;
gchar *lista[2];
GtkWidget *text1;
old = login;
text1 = gtk_text_new(NULL,NULL);
for (pos=0;pos<70;pos++) byte[pos] = 0;
pos = 0;
passwd = fopen("/etc/passwd","rb");
if (passwd) {
// pega o no de logins
while (!feof(passwd)) {fread(&x,1,1,passwd);if (x == 10) quantos++;}
fclose(passwd); 

// pega o 1o login
//fseek(passwd,0,SEEK_SET);
//pos = 0;
//do {fread(&byte[pos],1,1,passwd);pos++;} while (byte[pos-1] != ':');
//byte[pos-1] = 0;
//strcpy((char *)login,byte);

gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,"root",-1);
login = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen("root"));

//fclose(passwd); 
}

// looping principal (nao testa o primeiro, assumindo que eh o root)
for (loop=0;loop<(quantos-2);loop++) {
onde = find();
passwd = fopen("/etc/passwd","rb");
if (passwd) {
fseek(passwd,onde,SEEK_SET);
// proximo login
x = 0;
while (x != 10) {fread(&x,1,1,passwd);}
pos = 0;
do {fread(&byte[pos],1,1,passwd);pos++;} while (byte[pos-1] != ':');
byte[pos-1] = 0;
// strcpy((char *)login,byte);
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,byte,-1);
login = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen(login));
fclose(passwd);
id = pega_uid();
// Usuario Normal ?
if (id >= 1000 && id < 63000) {
if (!peguei) {
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,login,-1);
primeiro = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen(login));	  
peguei++;}
onde = find();
passwd = fopen("/etc/passwd","rb");
fseek(passwd,onde,SEEK_SET);
for (pos = 0; pos < 4; pos++) {
byte[0] = 0;
do {fread(&byte[0],1,1,passwd);} while (byte[0] != ':'); 
} // for
pos = 0;
do {fread(&byte[pos],1,1,passwd);pos++;} while (byte[pos-1] != ',');
byte[pos-1] = 0;
//strcpy(lista[0],login);
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,login,-1);
lista[0] = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen(login));
//strcpy(lista[1],byte);
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,byte,-1);
lista[1] = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen(byte));
gtk_clist_append(GTK_CLIST(clist1),lista);
fclose(passwd);
} // if uid
} // for quantos 
} // if passwd
if (first_time) {
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,primeiro,-1);
login = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1);
gtk_text_backward_delete(GTK_TEXT(text1),strlen(primeiro));	  
first_time = 0;
} else login = old;
} // lista login  

/////////////////////////////////////////////////////////////

void deluser(GtkWidget *x, gpointer y) {
char temp[40]="/bin/rm -rf /home/\0xxxxxxxxxxxxxxxxxxxxx";
int ret;
printf("\nErasing %s\n",login);
deline(find());
deline_s(find_s());

strcat(temp,login); //IMPORTANTISSIMO, SE COMENTAR JA ERA

printf("\nRunning : %s\n",temp);
gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
gtk_widget_show(princ);
ret=system(temp);
if (ret != -1 && ret != 127) mensagem("User deleted sucessfully ");
else mensagem("Erro deleting user ");
}

void grava(int pos,int s) {
if (pos!=-1) deline(pos);
passwd=fopen("/etc/passwd","r+b");
if (passwd) {
encontra(s);
fseek(passwd,0,SEEK_END);
fwrite(&login[0],strlen(login),1,passwd);
fwrite(&comp1[0],13,1,passwd);
fwrite(&nome[0],strlen(nome),1,passwd);         //nome
fwrite(&virgula,1,1,passwd);                 //virgula
fwrite(&endereco[0],strlen(endereco),1,passwd); //endereco
fwrite(&virgula,1,1,passwd);                 //virgula
fwrite(&tel1[0],strlen(tel1),1,passwd);         //tel1
fwrite(&virgula,1,1,passwd);                 //virgula
fwrite(&tel2[0],strlen(tel2),1,passwd);         //tel2
fwrite(&virgula,1,1,passwd);                 //virgula
fwrite(&coment[0],strlen(coment),1,passwd);     //coment
fwrite(&comp1[13],7,1,passwd);             
fwrite(&login[0],strlen(login),1,passwd);
fwrite(&comp2[0],11,1,passwd);
fclose(passwd);
}
}

void prof() {
FILE *profile;
char first[33] ="export PS1='\\h:\\w\\$ '\numask 022\n";
char startx[7]="startx\n";
char cria[30];
homedir();
strcpy(cria,home);
strcat(cria,"/.profile");
profile = fopen(cria,"wb");
if (profile) {
fwrite(&first,32,1,profile);
if (GTK_TOGGLE_BUTTON(checkbutton1)->active) 
fwrite(&startx[0],7,1,profile);
fclose(profile);
chmod(cria,S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP|S_IXGRP|S_IROTH|S_IWOTH|S_IXOTH);
}
}	  

void novos_dados(GtkWidget *x, gpointer y) {
int s;
s = pega_uid();
grava(find(),s);
//homedir();
//if (chown(home,pega_uid(),-1)) printf("\nError changing owner\n");
prof();
gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
gtk_widget_show(princ);
edit_flag=0;
}	  

void senha(GtkWidget *x, gpointer y) {
int k;
char msenha[50] = "/usr/X11R6/bin/xterm -e passwd \0xxxxxxxxxxxxxxxxxx";
if (strlen(login) > 0) {
strcat(msenha,login);
//printf("\n Senha Login : %s\n",login);
k = system(msenha);
if (k == -1 || k == 127)
{mensagem("\nErro changing password\n");return;}	  	    
if (criando) {
gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
gtk_widget_show(princ);
mensagem("User added sucessfully ");
criando=0;
return;
}
} else mensagem("You didn't choose an user");
gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
gtk_widget_show(princ);
}	  

void cria(GtkWidget *x, gpointer y) { 
ler_entrys();
if (!edit_flag) {
grava(-1,-1);
//grava_s(0,0);
mkhome();
prof();
create_senha();
//gtk_widget_show(msenha);

gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
}
else novos_dados(0,0);
}

void tchau(GtkWidget *x, gpointer y) {
gtk_main_quit();	  
}	  

void dados() {
char dado[100],x,ret=0;
int onde,i;
onde = find();
passwd = fopen("/etc/passwd","rb");
if (passwd) {
fseek(passwd,onde,SEEK_SET);

for (i=0;i<4;i++) {
x=0;
do { fread(&x,1,1,passwd);} while (x != ':') ;
}	  

//entry2
i=-1;
do {i++;fread(&dado[i],1,1,passwd);} while (dado[i] != ',' && dado[i] != ':');
if (dado[i] == ':') ret++; 
dado[i] = 0;
gtk_entry_set_text(GTK_ENTRY(entry2),dado);
if (ret) {fclose(passwd);return;} 
//entry3
i=-1;
do {i++;fread(&dado[i],1,1,passwd);} while (dado[i] != ',' && dado[i] != ':');
if (dado[i] == ':') ret++; 
dado[i] = 0;
gtk_entry_set_text(GTK_ENTRY(entry3),dado);
if (ret) {fclose(passwd);return;} 
//entry4
i=-1;
do {i++;fread(&dado[i],1,1,passwd);} while (dado[i] != ',' && dado[i] != ':');
if (dado[i] == ':') ret++; 
dado[i] = 0;
gtk_entry_set_text(GTK_ENTRY(entry4),dado);
if (ret) {fclose(passwd);return;} 
//entry5
i=-1;
do {i++;fread(&dado[i],1,1,passwd);} while (dado[i] != ',' && dado[i] != ':');
if (dado[i] == ':') ret++; 
dado[i] = 0;
gtk_entry_set_text(GTK_ENTRY(entry5),dado);
if (ret) {fclose(passwd);return;} 
//entry6
i=-1;
do {i++;fread(&dado[i],1,1,passwd);} while (dado[i] != ',' && dado[i] != ':');
if (dado[i] == ':') ret++;
dado[i] = 0;
gtk_entry_set_text(GTK_ENTRY(entry6),dado);
if (ret) {fclose(passwd);return;} 
fclose(passwd);
}	  
}	  
	  
GtkWidget*
create_window2 ()
{
  GtkWidget *window2;
  GtkWidget *fixed1;
  GtkWidget *hseparator1;
  GtkWidget *hseparator2;
/*
  GtkWidget *entry2;
  GtkWidget *entry3;
  GtkWidget *entry4;
  GtkWidget *entry5;
  GtkWidget *entry6;
*/
  GtkWidget *button1;
  GtkWidget *button2;
//  GtkWidget *checkbutton1;
  GtkWidget *label2;
  GtkWidget *label1;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *label7;

  window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window2), "window2", window2);
  gtk_widget_set_usize (window2, 410, 320);
  gtk_window_set_title (GTK_WINDOW (window2), "Finger");
  gtk_window_set_policy (GTK_WINDOW (window2), TRUE, TRUE, FALSE);

  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (window2), fixed1);

  hseparator1 = gtk_hseparator_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "hseparator1", hseparator1);
  gtk_widget_show (hseparator1);
  gtk_fixed_put (GTK_FIXED (fixed1), hseparator1, 0, 40);
  gtk_widget_set_usize (hseparator1, 410, 10);

  hseparator2 = gtk_hseparator_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "hseparator2", hseparator2);
  gtk_widget_show (hseparator2);
  gtk_fixed_put (GTK_FIXED (fixed1), hseparator2, 0, 260);
  gtk_widget_set_usize (hseparator2, 410, 10);

  entry2 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "entry2", entry2);
  gtk_widget_show (entry2);
  gtk_fixed_put (GTK_FIXED (fixed1), entry2, 110, 74);
  gtk_widget_set_usize (entry2, 295, 21);

  entry3 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "entry3", entry3);
  gtk_widget_show (entry3);
  gtk_fixed_put (GTK_FIXED (fixed1), entry3, 110, 106);
  gtk_widget_set_usize (entry3, 295, 21);

  entry4 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "entry4", entry4);
  gtk_widget_show (entry4);
  gtk_fixed_put (GTK_FIXED (fixed1), entry4, 110, 138);
  gtk_widget_set_usize (entry4, 150, 21);

  entry5 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "entry5", entry5);
  gtk_widget_show (entry5);
  gtk_fixed_put (GTK_FIXED (fixed1), entry5, 110, 170);
  gtk_widget_set_usize (entry5, 150, 21);

  entry6 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "entry6", entry6);
  gtk_widget_show (entry6);
  gtk_fixed_put (GTK_FIXED (fixed1), entry6, 110, 202);
  gtk_widget_set_usize (entry6, 295, 21);

  button1 = gtk_button_new_with_label ("OK");
  gtk_object_set_data (GTK_OBJECT (window2), "button1", button1);
  gtk_widget_show (button1);
  gtk_fixed_put (GTK_FIXED (fixed1), button1, 104, 279);
  gtk_widget_set_usize (button1, 95, 24);
  gtk_signal_connect (GTK_OBJECT (button1), "clicked",
                      GTK_SIGNAL_FUNC (cria),
                      NULL);
  gtk_signal_connect_object (GTK_OBJECT (button1), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (window2));

  button2 = gtk_button_new_with_label ("Cancel");
  gtk_object_set_data (GTK_OBJECT (window2), "button2", button2);
  gtk_widget_show (button2);
  gtk_fixed_put (GTK_FIXED (fixed1), button2, 216, 279);
  gtk_widget_set_usize (button2, 95, 24);
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_show),
                             GTK_OBJECT (princ));
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (c_criando),
                             GTK_OBJECT (window2));

  checkbutton1 = gtk_check_button_new_with_label ("Enter graphic mode automatically");
  gtk_object_set_data (GTK_OBJECT (window2), "checkbutton1", checkbutton1);
  gtk_widget_show (checkbutton1);
  gtk_fixed_put (GTK_FIXED (fixed1), checkbutton1, 15, 234);
  gtk_widget_set_usize (checkbutton1, 379, 23);

  label2 = gtk_label_new (login);
  gtk_object_set_data (GTK_OBJECT (window2), "label2", label2);
  gtk_widget_show (label2);
  gtk_fixed_put (GTK_FIXED (fixed1), label2, 0, 25);
  gtk_widget_set_usize (label2, 400, 15);

  label1 = gtk_label_new ("Changing finger information of :");
  gtk_object_set_data (GTK_OBJECT (window2), "label1", label1);
  gtk_widget_show (label1);
  gtk_fixed_put (GTK_FIXED (fixed1), label1, 0, 8);
  gtk_widget_set_usize (label1, 400, 15);

  label3 = gtk_label_new ("Name");
  gtk_object_set_data (GTK_OBJECT (window2), "label3", label3);
  gtk_widget_show (label3);
  gtk_fixed_put (GTK_FIXED (fixed1), label3, 0, 82);
  gtk_widget_set_usize (label3, 112, 16);

  label4 = gtk_label_new ("Locality");
  gtk_object_set_data (GTK_OBJECT (window2), "label4", label4);
  gtk_widget_show (label4);
  gtk_fixed_put (GTK_FIXED (fixed1), label4, 0, 114);
  gtk_widget_set_usize (label4, 112, 16);

  label5 = gtk_label_new ("Word Phone");
  gtk_object_set_data (GTK_OBJECT (window2), "label5", label5);
  gtk_widget_show (label5);
  gtk_fixed_put (GTK_FIXED (fixed1), label5, 0, 146);
  gtk_widget_set_usize (label5, 112, 16);

  label6 = gtk_label_new ("Home Phone");
  gtk_object_set_data (GTK_OBJECT (window2), "label6", label6);
  gtk_widget_show (label6);
  gtk_fixed_put (GTK_FIXED (fixed1), label6, 0, 178);
  gtk_widget_set_usize (label6, 112, 16);

  label7 = gtk_label_new ("Coment");
  gtk_object_set_data (GTK_OBJECT (window2), "label7", label7);
  gtk_widget_show (label7);
  gtk_fixed_put (GTK_FIXED (fixed1), label7, 0, 210);
  gtk_widget_set_usize (label7, 112, 16);

  if (!criando) dados();
    
  return window2;
}

void show(GtkWidget *x, gint row, gint col, GdkEventButton *event, gpointer y) {
gtk_clist_get_text(GTK_CLIST(clist1),row,0,&login);	  
//printf("\n%s\n",login);
}	  
         
void Edit_pressed(GtkWidget *x, gpointer y) {
edit_flag = 1;	  
window2 = create_window2();
gtk_widget_show(window2);
gtk_widget_hide(princ);
}	  

	  
GtkWidget*
create_main ()
{
  GtkWidget *main;
  GtkWidget *fixed1;
//  GtkWidget *clist1;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *Adicionar;
  GtkWidget *Remover;
//  GtkWidget *Editar;
  GtkWidget *Senha;
  GtkWidget *Sair;
  GtkWidget *vsc;
  
  main = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (main), "main", main);
  gtk_widget_set_usize (main, 400, 200);
  gtk_window_set_title (GTK_WINDOW (main), "User editor");
  gtk_window_set_policy (GTK_WINDOW (main), FALSE, FALSE, FALSE);
  gtk_signal_connect (GTK_OBJECT (main), "destroy",
                      GTK_SIGNAL_FUNC (tchau),
                      NULL);
  
  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (main), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (main), fixed1);

  clist1 = gtk_clist_new (2);
  gtk_object_set_data (GTK_OBJECT (main), "clist1", clist1);
  gtk_widget_show (clist1);  
  // gtk_fixed_put (GTK_FIXED (fixed1), clist1, 0, 0);
  gtk_widget_set_usize (clist1, 300, 198);
  gtk_clist_set_column_width (GTK_CLIST (clist1), 0, 80);
  gtk_clist_set_column_width (GTK_CLIST (clist1), 1, 80);
  gtk_clist_column_titles_show (GTK_CLIST (clist1));
  gtk_clist_set_shadow_type (GTK_CLIST (clist1), GTK_SHADOW_NONE);
  gtk_signal_connect (GTK_OBJECT (clist1), "select_row",
                      GTK_SIGNAL_FUNC (show),
                      NULL); 
//  vsc = gtk_vscrollbar_new(GTK_CLIST(clist1)->vadjustment);
  vsc=gtk_scrolled_window_new(NULL,NULL);
  gtk_widget_show(vsc);
//  gtk_fixed_put (GTK_FIXED (fixed1), vsc, 300, 0);
//  gtk_widget_set_usize (vsc, 0, 198);  
  
  gtk_container_add(GTK_CONTAINER(vsc),clist1);
  gtk_fixed_put (GTK_FIXED (fixed1), vsc, 0, 0);
  gtk_widget_set_usize (vsc, 300, 198);

  
  //TESTE
  
  label1 = gtk_label_new ("Login");
  gtk_object_set_data (GTK_OBJECT (main), "label1", label1);
  gtk_widget_show (label1);
  gtk_clist_set_column_widget (GTK_CLIST (clist1), 0, label1);
  gtk_widget_set_usize (label1, 36, -1);

  label2 = gtk_label_new ("Name");
  gtk_object_set_data (GTK_OBJECT (main), "label2", label2);
  gtk_widget_show (label2);
  gtk_clist_set_column_widget (GTK_CLIST (clist1), 1, label2);
  gtk_widget_set_usize (label2, 34, -1);

  Adicionar = gtk_button_new_with_label ("Add");
  gtk_object_set_data (GTK_OBJECT (main), "Adicionar", Adicionar);
  gtk_widget_show (Adicionar);
  gtk_fixed_put (GTK_FIXED (fixed1), Adicionar, 315, 22);
  gtk_widget_set_usize (Adicionar, 78, 24);
  gtk_signal_connect (GTK_OBJECT (Adicionar), "clicked",
                      GTK_SIGNAL_FUNC (r_login_win),
                      NULL);

  Remover = gtk_button_new_with_label ("Remove");
  gtk_object_set_data (GTK_OBJECT (main), "Remover", Remover);
  gtk_widget_show (Remover);
  gtk_fixed_put (GTK_FIXED (fixed1), Remover, 315, 46);
  gtk_widget_set_usize (Remover, 78, 24);
  gtk_signal_connect (GTK_OBJECT (Remover), "clicked",
                      GTK_SIGNAL_FUNC (r_remove),
                      NULL);
  
  Editar = gtk_button_new_with_label ("Edit");
  gtk_object_set_data (GTK_OBJECT (main), "Editar", Editar);
  gtk_widget_show (Editar);
  gtk_fixed_put (GTK_FIXED (fixed1), Editar, 315, 70);
  gtk_widget_set_usize (Editar, 78, 24);
  gtk_signal_connect (GTK_OBJECT (Editar), "clicked",
                      GTK_SIGNAL_FUNC (Edit_pressed),
                      GTK_OBJECT(Editar));
  
  Senha = gtk_button_new_with_label ("Password");
  gtk_object_set_data (GTK_OBJECT (main), "Senha", Senha);
  gtk_widget_show (Senha);
  gtk_fixed_put (GTK_FIXED (fixed1), Senha, 315, 94);
  gtk_widget_set_usize (Senha, 78, 24);
// Doesn't work
//  gtk_signal_connect (GTK_OBJECT (Senha), "clicked",
//                      GTK_SIGNAL_FUNC (gtk_widget_hide),
//                      GTK_OBJECT (main));
  gtk_signal_connect (GTK_OBJECT (Senha), "clicked",
                      GTK_SIGNAL_FUNC (create_senha),
                      NULL);

  
  Sair = gtk_button_new_with_label ("Exit");
  gtk_object_set_data (GTK_OBJECT (main), "Sair", Sair);
  gtk_widget_show (Sair);
  gtk_fixed_put (GTK_FIXED (fixed1), Sair, 315, 136);
  gtk_widget_set_usize (Sair, 78, 24);
  gtk_signal_connect (GTK_OBJECT (Sair), "clicked",
                      GTK_SIGNAL_FUNC (tchau),
                      NULL);
  lista_login();
  return main;
}

GtkWidget*
create_Remove ()
{
  GtkWidget *Remove;
  GtkWidget *fixed1;
  GtkWidget *Sim;
  GtkWidget *Nao;
  GtkWidget *remov;
  char p[98],i;
  char p1[] = "The directory \0";
  char p2[] = " and it's contents will be lost.\nContinue?\0";
  for (i=0;i<98;i++) p[i] = 0;
  homedir();
  strcat(p,p1);
  strcat(p,home);
  strcat(p,p2);

  gtk_widget_hide(princ);
  //printf("\n%s\n",p);
  
  Remove = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (Remove), "Remove", Remove);
  gtk_widget_set_usize (Remove, 340, 80);
  gtk_window_set_title (GTK_WINDOW (Remove), "Remove?");
  gtk_window_set_policy (GTK_WINDOW (Remove), TRUE, TRUE, FALSE);

  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (Remove), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (Remove), fixed1);

  Sim = gtk_button_new_with_label ("Yes");
  gtk_object_set_data (GTK_OBJECT (Remove), "Sim", Sim);
  gtk_widget_show (Sim);
  gtk_fixed_put (GTK_FIXED (fixed1), Sim, 89, 48);
  gtk_widget_set_usize (Sim, 74, 21);
  gtk_signal_connect_object (GTK_OBJECT (Sim), "clicked",
                             GTK_SIGNAL_FUNC (deluser),
                             NULL);
  gtk_signal_connect_object (GTK_OBJECT (Sim), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (Remove));
  Nao = gtk_button_new_with_label ("No");
  gtk_object_set_data (GTK_OBJECT (Remove), "Nao", Nao);
  gtk_widget_show (Nao);
  gtk_fixed_put (GTK_FIXED (fixed1), Nao, 184, 48);
  gtk_widget_set_usize (Nao, 74, 21);
  gtk_signal_connect_object (GTK_OBJECT (Nao), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_show),
                             GTK_OBJECT (princ));
  gtk_signal_connect_object (GTK_OBJECT (Nao), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (Remove));

  remov = gtk_label_new (p);
  gtk_object_set_data (GTK_OBJECT (Remove), "remov", remov);
  gtk_widget_show (remov);
  gtk_fixed_put (GTK_FIXED (fixed1), remov, 0, 5);
  gtk_widget_set_usize (remov, 340, 35);

  return Remove;
}

void r_remove(GtkWidget *x, gpointer y) {
window4 = create_Remove();
gtk_widget_show(window4);
}

void ver_senha(GtkWidget *x, gpointer y) {
char *j,*k;
int n;
j = gtk_entry_get_text(GTK_ENTRY(sentry1));
k = gtk_entry_get_text(GTK_ENTRY(sentry2)); 
if (!strcmp(j,k)) {
//printf("\nGerando com : %s\n",j);
//gera_senha(j);
n = find_s();
if (n != -1) deline_s(find_s());
grava_s(1,j);
if (criando) {
gtk_clist_clear(GTK_CLIST(clist1));
lista_login();
gtk_widget_show(princ);
mensagem("User added sucessfully ");
criando=0;
} // if criando
gtk_widget_show(princ);
gtk_widget_destroy(x);
} // if j == k
else {mensagem("Password mismatch");return;}
//gtk_entry_delete_text(GTK_ENTRY(sentry1),0,-1);
//gtk_entry_delete_text(GTK_ENTRY(sentry2),0,-1);
//if (j) g_free(j);
//if (k) g_free(k);
}	
	
void
create_senha ()
{
  GtkWidget *window1;
  GtkWidget *fixed1;
  GtkWidget *label2;
  GtkWidget *label3;
//  GtkWidget *entry1;
//  GtkWidget *entry2;
  GtkWidget *button1;
  GtkWidget *label4;
  GtkWidget *label5;

  gtk_widget_hide(princ);

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window1), "window1", window1);
  gtk_widget_set_usize (window1, 300, 150); 
  gtk_window_set_title (GTK_WINDOW (window1), "Password");
  gtk_window_set_policy (GTK_WINDOW (window1), TRUE, TRUE, FALSE);
//  gtk_signal_connect (GTK_OBJECT (window1), "destroy",
//                      GTK_SIGNAL_FUNC (tchau),
//                      NULL);

  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (window1), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (window1), fixed1);

  label2 = gtk_label_new ("Changing password of :");
  gtk_object_set_data (GTK_OBJECT (window1), "label2", label2);
  gtk_widget_show (label2);
  gtk_fixed_put (GTK_FIXED (fixed1), label2, 0, 0);
  gtk_widget_set_usize (label2, 298, 16);

  label3 = gtk_label_new (login);
  gtk_object_set_data (GTK_OBJECT (window1), "label3", label3);
  gtk_widget_show (label3);
  gtk_fixed_put (GTK_FIXED (fixed1), label3, 0, 16);
  gtk_widget_set_usize (label3, 298, 16);

  sentry1 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window1), "entry1", sentry1);
  gtk_entry_set_visibility(GTK_ENTRY(sentry1),FALSE);
  gtk_widget_show (sentry1);
  gtk_fixed_put (GTK_FIXED (fixed1), sentry1, 112, 48);
  gtk_widget_set_usize (sentry1, 158, 22);

  sentry2 = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window1), "entry2", sentry2);
  gtk_entry_set_visibility(GTK_ENTRY(sentry2),FALSE);
  gtk_widget_show (sentry2);
  gtk_fixed_put (GTK_FIXED (fixed1), sentry2, 112, 80);
  gtk_widget_set_usize (sentry2, 158, 22);

  button1 = gtk_button_new_with_label ("OK");
  gtk_object_set_data (GTK_OBJECT (window1), "button1", button1);
  gtk_widget_show (button1);
  gtk_fixed_put (GTK_FIXED (fixed1), button1, 105, 115);
  gtk_widget_set_usize (button1, 94, 24);
  gtk_signal_connect_object (GTK_OBJECT (button1), "released",
                             GTK_SIGNAL_FUNC (ver_senha),
                             GTK_OBJECT(window1));
 
  label4 = gtk_label_new ("Enter password");
  gtk_object_set_data (GTK_OBJECT (window1), "label4", label4);
  gtk_widget_show (label4);
  gtk_fixed_put (GTK_FIXED (fixed1), label4, 0, 56);
  gtk_widget_set_usize (label4, 112, 16);

  label5 = gtk_label_new ("Reenter password");
  gtk_object_set_data (GTK_OBJECT (window1), "label5", label5);
  gtk_widget_show (label5);
  gtk_fixed_put (GTK_FIXED (fixed1), label5, 0, 88);
  gtk_widget_set_usize (label5, 112, 16);

  gtk_widget_show(window1);
}
	  
