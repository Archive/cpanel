

#include "netsupport.h"

void show_msg_dialog_clicked(GtkWidget * dialog, gint button,
                             gpointer data)
{
   gnome_dialog_close(GNOME_DIALOG(dialog));
}


void show_msg(char *msg)
{
  GtkWidget *dialog;
  GtkWidget *hbox1;
  GdkPixmap *glade_pixmap;
  GdkBitmap *glade_mask;
  GtkWidget *pixmap1;
  GtkWidget *label2;

  dialog = gnome_dialog_new(_("Message"), GNOME_STOCK_BUTTON_OK,NULL);

  gtk_signal_connect(GTK_OBJECT(dialog), "clicked",
                                GTK_SIGNAL_FUNC(show_msg_dialog_clicked),
                                NULL);
                  
  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox1);
  gtk_container_border_width (GTK_CONTAINER (hbox1), 10);

  glade_pixmap = gdk_pixmap_create_from_xpm (dialog->window, &glade_mask,
                                             &dialog->style->bg[GTK_STATE_NORMAL],
                                             "/etc/cpanel/icons/network.xpm");
  pixmap1 = gtk_pixmap_new (glade_pixmap, glade_mask);
  gdk_pixmap_unref (glade_pixmap);
  gdk_bitmap_unref (glade_mask);
  gtk_widget_show (pixmap1);
  gtk_box_pack_start (GTK_BOX (hbox1), pixmap1, TRUE, TRUE, 10);

  label2 = gtk_label_new (msg);
  gtk_widget_show (label2);
  gtk_box_pack_start (GTK_BOX (hbox1), label2, TRUE, TRUE, 10);

  gtk_box_pack_start(GNOME_DIALOG(dialog)->vbox,hbox1, TRUE, TRUE, GNOME_PAD);

  gnome_dialog_run(GNOME_DIALOG(dialog));

}

GtkWidget *entry4;
GtkWidget *entry5;
GtkWidget *entry6;


void get_pars_clicked_cb(GnomeDialog * dialog, gint button_number, 
                                     gpointer data)
{

  if (button_number == 0)
  {
	  char command[200];
	  int  retcode;

	  strcpy(command,"/sbin/modprobe ");
	  strcat(command,data);
	  strcat(command," ");
	  if (strcmp(gtk_entry_get_text(GTK_WIDGET(entry4)),"") != 0)
	  {
		  strcat(command," io=");
		  strcat(command,gtk_entry_get_text(GTK_WIDGET(entry4)));
	  }
	  if (strcmp(gtk_entry_get_text(GTK_WIDGET(entry5)),"") != 0)
	  {
		  strcat(command," irq=");
		  strcat(command,gtk_entry_get_text(GTK_WIDGET(entry5)));
	  }
	  if (strcmp(gtk_entry_get_text(GTK_WIDGET(entry6)),"") != 0)
	  {
		  strcat(command," ");
		  strcat(command,gtk_entry_get_text(GTK_WIDGET(entry6)));
	  }

	  printf("run %s \n",command);
	  
	  retcode = system(command);
	  
	  if (retcode)
		  show_msg("I couldn't load the module. ");
	  else
	  {
		  if (find_bword(data,"/etc/modules") == 0)
		  {
		  	  FILE *mods;

			  mods = fopen("/etc/modules","a");
			  fprintf(mods,"%s\n",data);
		  }

		  show_msg("Module installed successfully. ");
	  }
  }
	
  gnome_dialog_close(dialog);
}



void get_pars(char *str)
{
  GtkWidget *dialog;
  GtkWidget *hbox1;
  GtkWidget *vbox1;
  GtkWidget *hbox2;
  GdkPixmap *glade_pixmap;
  GdkBitmap *glade_mask;
  GtkWidget *pixmap3;
  GtkWidget *label8;
  GtkWidget *table3;
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *label7;
  FILE      *zcat;
  char      labeltext[1024];
  char      buff[1024];


  zcat = popen("zcat /usr/lib/module_help/descr.gz","r");
  if (zcat == NULL)
	  show_msg("Couldn't open the descriptions file. ");

  //strcpy(buff,"");
  while (strstr(buff,str) == NULL)
	  fgets(buff,1024,zcat);

  if (strstr(buff,str) != NULL)
  {
	  fgets(buff,1024,zcat);
	  fgets(buff,1024,zcat);
	  strcpy(labeltext,buff);
	  while (buff[0] == ' ')
	  {
		  fgets(buff,1024,zcat);
	  	  strcat(labeltext,buff);
	  }
  }
  else
     strcpy(labeltext,"No description available. ");

  pclose(zcat);



  dialog = gnome_dialog_new(_("Message"), GNOME_STOCK_BUTTON_OK,GNOME_STOCK_BUTTON_CANCEL,NULL);
                  
  gtk_signal_connect(GTK_OBJECT(dialog), "clicked",
                      GTK_SIGNAL_FUNC(get_pars_clicked_cb),
                      str);


  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox1);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gtk_box_pack_start (GTK_BOX (hbox1), vbox1, TRUE, TRUE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox2, TRUE, TRUE, 0);

  glade_pixmap = gdk_pixmap_create_from_xpm (dialog->window, &glade_mask,
                                             &dialog->style->bg[GTK_STATE_NORMAL],
                                             "/etc/cpanel/icons/network.xpm");
  pixmap3 = gtk_pixmap_new (glade_pixmap, glade_mask);
  gdk_pixmap_unref (glade_pixmap);
  gdk_bitmap_unref (glade_mask);
  gtk_widget_show (pixmap3);
  gtk_box_pack_start (GTK_BOX (hbox2), pixmap3, FALSE, TRUE, 15);

  label8 = gtk_label_new (labeltext);
  gtk_widget_show (label8);
  gtk_box_pack_start (GTK_BOX (hbox2), label8, TRUE, TRUE, 0);
  gtk_label_set_justify (GTK_LABEL (label8), GTK_JUSTIFY_LEFT);

  table3 = gtk_table_new (3, 2, FALSE);
  gtk_widget_show (table3);
  gtk_box_pack_start (GTK_BOX (vbox1), table3, TRUE, TRUE, 0);
  gtk_container_border_width (GTK_CONTAINER (table3), 10);

  entry4 = gtk_entry_new ();
  gtk_widget_show (entry4);
  gtk_table_attach (GTK_TABLE (table3), entry4, 1, 2, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  entry5 = gtk_entry_new ();
  gtk_widget_show (entry5);
  gtk_table_attach (GTK_TABLE (table3), entry5, 1, 2, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  entry6 = gtk_entry_new ();
  gtk_widget_show (entry6);
  gtk_table_attach (GTK_TABLE (table3), entry6, 1, 2, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label5 = gtk_label_new ("IO :");
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table3), label5, 0, 1, 0, 1,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label6 = gtk_label_new ("IRQ : ");
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table3), label6, 0, 1, 1, 2,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  label7 = gtk_label_new ("Extra :");
  gtk_widget_show (label7);
  gtk_table_attach (GTK_TABLE (table3), label7, 0, 1, 2, 3,
                    (GtkAttachOptions) GTK_EXPAND | GTK_FILL, (GtkAttachOptions) GTK_EXPAND | GTK_FILL, 0, 0);

  gtk_box_pack_start(GNOME_DIALOG(dialog)->vbox,hbox1, TRUE, TRUE, GNOME_PAD);

  gtk_widget_set_usize(GTK_WIDGET(dialog),500,300);

  gtk_widget_show(dialog);

}

void install_module(char *str)
{
	char command[200];
	int  retcode;
	printf("install_module()\n");

	strcpy(command,"/sbin/modprobe ");
	strcat(command,str);

	printf("install_module(%s) : %s\n",str,command);

	retcode = system(command);

	if (retcode)
	{
		get_pars(str);
	}
	else
	{
		if (find_bword(str,"/etc/modules") == 0)
		{
			FILE *mods;

			mods = fopen("/etc/modules","a");
			fprintf(mods,"%s\n",str);
		}
		show_msg("Device configured successfuly!!!");
	}
}

void remove_module_dialog_clicked(GtkWidget * dialog, gint button,
                             gpointer data)
{
  if (button == 0)
	{
		char command[200];
		int  retcode;

		strcpy(command,"modprobe -r ");
		strcat(command,data);

		retcode = system(command);
		if (retcode)
			show_msg("Error trying to remove module. ");
		else
			show_msg("Module removed sucessfully. ");
	}


   gnome_dialog_close(GNOME_DIALOG(dialog));
}


void remove_module(char *str)
{

  GtkWidget *dialog;
  GtkWidget *hbox1;
  GdkPixmap *glade_pixmap;
  GdkBitmap *glade_mask;
  GtkWidget *pixmap1;
  GtkWidget *label2;

  dialog = gnome_dialog_new(_("Message"), GNOME_STOCK_BUTTON_YES,GNOME_STOCK_BUTTON_NO,NULL);

  gtk_signal_connect(GTK_OBJECT(dialog), "clicked",
                                GTK_SIGNAL_FUNC(remove_module_dialog_clicked),
                                str);
                  
  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox1);
  gtk_container_border_width (GTK_CONTAINER (hbox1), 10);

  glade_pixmap = gdk_pixmap_create_from_xpm (dialog->window, &glade_mask,
                                             &dialog->style->bg[GTK_STATE_NORMAL],
                                             "/etc/cpanel/icons/network.xpm");
  pixmap1 = gtk_pixmap_new (glade_pixmap, glade_mask);
  gdk_pixmap_unref (glade_pixmap);
  gdk_bitmap_unref (glade_mask);
  gtk_widget_show (pixmap1);
  gtk_box_pack_start (GTK_BOX (hbox1), pixmap1, TRUE, TRUE, 10);

  label2 = gtk_label_new ("Module is installed, do you want to remove it?");
  gtk_widget_show (label2);
  gtk_box_pack_start (GTK_BOX (hbox1), label2, TRUE, TRUE, 10);

  gtk_box_pack_start(GNOME_DIALOG(dialog)->vbox,hbox1, TRUE, TRUE, GNOME_PAD);

  gnome_dialog_run(GNOME_DIALOG(dialog));
}
