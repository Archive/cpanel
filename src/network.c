/*  Network  
 *  Copyright (C) 1999   Diego Delgado Lages <lages@linuxlabs.lci.ufrj.br>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

//#include <gtk/gtk.h>
#include "netsupport.h"
//#include <gnome.h>
#include <stdio.h>
#include <dirent.h>
#include "utils.h"

#include "icons/yes.xpm"
#include "icons/no.xpm"

gchar *titles[2] = {"Type","Devices"};
gchar *addtitle[3] = {" ","Card","Description"};

void add_partitions(); 

void create_main_window ();

void config_dev();

GtkStyle *style;
GtkPixmap *pyes;
GtkPixmap *pno;
GdkBitmap *yesmask;
GdkBitmap *nomask;


void main (int argc, char *argv[])
{
	GtkStyle *style;
	GtkPixmap *pyes;
   GtkPixmap *pno;
	GdkBitmap *yesmask;
	GdkBitmap *nomask;
	
	gtk_set_locale();

	//gtk_init (&argc,&argv);
	  
	gnome_init("Network Configuration","0.0.1",argc,&argv);

   create_main_window();

	gtk_main();

}

void create_main_window ()
{
  GtkWidget *window;
  GtkWidget *hbox1;
  GtkWidget *hbox2;
  GtkWidget *hbox3;
  GtkWidget *vbox1;
  GtkWidget *vbox2;
  GtkWidget *vbox3;
  GtkWidget *vbox4;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *ldevice;
  GtkWidget *entry1;
  GtkWidget *entry2;
  GtkWidget *entry3;
  GtkWidget *entry4;
  GtkWidget *entry5;
  GtkWidget *bok;
  GtkWidget *bcancel;
  GtkWidget *badd;
  GtkWidget *bedit;
  GtkWidget *bdel;
  GtkWidget *separator1;
  GtkWidget *separator2;
  GtkWidget *clist; 
  FILE      *netconf;
  char      buff[100];
  char      *items[2][2];   

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Network configuration");


  style = gtk_widget_get_style(window);
  pyes = gdk_pixmap_create_from_xpm_d(window->window, &yesmask,
		  											&style->bg[GTK_STATE_NORMAL],
													(gchar **)yes);

  pno = gdk_pixmap_create_from_xpm_d(window->window, &nomask,
		  											&style->bg[GTK_STATE_NORMAL],
													(gchar **)no);




  
/* window <- vbox3
 * vbox3 <- hbox3, separator2, hbox1, separator1, hbox2 
 * hbox1 <- vbox1, vbox2
 * vbox1 <- label ....
 * vbox2 <- entry .... */
  vbox3 = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vbox3);
  gtk_container_add (GTK_CONTAINER(window),vbox3);


  ldevice = gtk_label_new("Device : ");
  gtk_widget_show(ldevice);
  gtk_box_pack_start(GTK_BOX(vbox3),ldevice, FALSE,TRUE,10);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show(hbox3);
  gtk_box_pack_start(GTK_BOX(vbox3),hbox3,FALSE,TRUE,10);

  clist = gtk_clist_new_with_titles(2,titles);
  gtk_widget_show(clist);
  gtk_container_add(GTK_CONTAINER(hbox3),clist);

  netconf = fopen("/etc/cpanel/network","r");
  if (netconf)
  {
	  char type[20];
	  char desc[200];
	  fgets(type,20,netconf);

	  while (!feof(netconf))
	  {
		  fgets(buff,100,netconf);
		  fgets(desc,200,netconf);
		  fgets(buff,100,netconf);
		  fgets(buff,100,netconf);
		  fgets(buff,100,netconf);
		  fgets(buff,100,netconf);
		  items[0][0]=type;
		  items[0][1]=desc;
		  gtk_clist_append(GTK_CLIST(clist),items[0]);
	  	  fgets(type,20,netconf);
	  }
  }
	  

  vbox4 = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vbox4);
  gtk_box_pack_start(GTK_BOX(hbox3),vbox4,FALSE,FALSE,10);

  badd = gtk_button_new_with_label("Add...");
  gtk_widget_show(badd);
  gtk_box_pack_start(GTK_BOX(vbox4),badd,TRUE, TRUE, 0);

  bedit = gtk_button_new_with_label("Edit...");
  gtk_widget_show(bedit);
  gtk_box_pack_start(GTK_BOX(vbox4),bedit,TRUE, TRUE, 0);

  bdel = gtk_button_new_with_label("Delete...");
  gtk_widget_show(bdel);
  gtk_box_pack_start(GTK_BOX(vbox4),bdel,TRUE, TRUE, 0);


  
  separator2 = gtk_hseparator_new();
  gtk_widget_show(separator2);
  gtk_box_pack_start(GTK_BOX(vbox3),separator2,FALSE,TRUE,20);

  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox1);
  gtk_box_pack_start(GTK_BOX(vbox3),hbox1,FALSE,TRUE,0);

  separator1 = gtk_hseparator_new();
  gtk_widget_show(separator1);
  gtk_box_pack_start(GTK_BOX(vbox3),separator1,FALSE,TRUE,20);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show(hbox2);
  gtk_box_pack_start(GTK_BOX(vbox3),hbox2,FALSE, TRUE, 10);

  bok = gtk_button_new_with_label("Ok");
  gtk_widget_show(bok);
  gtk_box_pack_start(GTK_BOX(hbox2),bok,TRUE, TRUE, 10);

  bcancel = gtk_button_new_with_label("Cancel");
  gtk_widget_show(bcancel);
  gtk_box_pack_start(GTK_BOX(hbox2),bcancel,TRUE, TRUE,10);

  vbox1 = gtk_vbox_new (FALSE, 0);	
  gtk_widget_show (vbox1);
  gtk_box_pack_start(GTK_BOX(hbox1),vbox1,FALSE,TRUE,10);

  vbox2 = gtk_vbox_new (FALSE, 0);	
  gtk_widget_show (vbox2);
  gtk_box_pack_start(GTK_BOX(hbox1),vbox2,FALSE,TRUE,10);

  label1 = gtk_label_new("IP : ");
  label2 = gtk_label_new("Netmask : ");
  label3 = gtk_label_new("Network : ");
  label4 = gtk_label_new("Broadcast : ");
  label5 = gtk_label_new("Gateway : ");

  entry1 = gtk_entry_new_with_max_length(15);
  entry2 = gtk_entry_new_with_max_length(15);
  entry3 = gtk_entry_new_with_max_length(15);
  entry4 = gtk_entry_new_with_max_length(15);
  entry5 = gtk_entry_new_with_max_length(15);

  gtk_widget_set_usize(label1,-1,20);
  gtk_widget_set_usize(label2,-1,20);
  gtk_widget_set_usize(label3,-1,20);
  gtk_widget_set_usize(label4,-1,20);
  gtk_widget_set_usize(label5,-1,20);
  gtk_widget_set_usize(entry1,-1,20);
  gtk_widget_set_usize(entry2,-1,20);
  gtk_widget_set_usize(entry3,-1,20);
  gtk_widget_set_usize(entry4,-1,20);
  gtk_widget_set_usize(entry5,-1,20);

  gtk_box_pack_start(GTK_BOX(vbox1),label1, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox1),label2, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox1),label3, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox1),label4, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox1),label5, TRUE,TRUE,0);

  gtk_box_pack_start(GTK_BOX(vbox2),entry1, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox2),entry2, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox2),entry3, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox2),entry4, TRUE,TRUE,0);
  gtk_box_pack_start(GTK_BOX(vbox2),entry5, TRUE,TRUE,0);

  gtk_signal_connect(GTK_OBJECT(window),"destroy",
                     GTK_SIGNAL_FUNC(gtk_main_quit),NULL);

  gtk_signal_connect(GTK_OBJECT(badd),"clicked",
		  					GTK_SIGNAL_FUNC(config_dev),NULL);

  gtk_widget_show(label1);
  gtk_widget_show(label2);
  gtk_widget_show(label3);
  gtk_widget_show(label4);
  gtk_widget_show(label5);

  gtk_widget_show(entry1);
  gtk_widget_show(entry2);
  gtk_widget_show(entry3);
  gtk_widget_show(entry4);
  gtk_widget_show(entry5);


  gtk_widget_show(vbox1);

  gtk_widget_show(window);
  
}

GtkWidget *dev_clist;

int detect_netcards()
{
	FILE      *isapnp;
	char      buff[1024];
	int       i;
	char 	    s[100];
	GtkWidget *m;

	if (find_word("(CONFIGURE","/etc/isapnp.conf") == NULL)
	{
		printf("We don't have plug and play devices. \n");
		return;
	}

	isapnp = fopen ("/etc/isapnp.conf","r");

	while (!feof(isapnp))
	{
		char id[9];
		FILE *isalst;
		FILE *netconf;
		char b[1024];


		strcpy(buff,"");
		while ((strncmp(buff,"(CONFIGURE",10) != 0) && (!feof(isapnp)))
			fgets(buff,1024,isapnp);

		if (feof(isapnp))
			continue;

		strcpy(id,&buff[11]);

		id[7]='\0'; /* Now id has the ID of the actual device. */

		/* See if we have already configured such device. */

		netconf = fopen("/etc/cpanel/network","r");

		if (netconf)
		{
			int configured=0;
			while (!feof(netconf))
			{
				b[100];
				fscanf(netconf,"%s",b);
				if (strcmp(b,"ISA") == 0)
				{
				   fscanf(netconf,"%s",b);
					if (strcmp(id,b) == 0)
						configured++;
				}
			}

			fclose(netconf);
			
			if (configured)
			{
				printf("Already configured. \n");
				continue;
			}
		}


		printf("Found plug and play device : %s \n",id); /* Self said. */

		isalst = fopen("/etc/isa.lst","r");

		if (isalst == NULL)
		{
			show_msg("I couldn't open the Plug and Play Database. \n");
			exit(0);
		}

		while (!feof(isalst))
		{	
			char isaid[100];
			
			fgets(b,1024,isalst);

		   if (strncmp(b,id,7) == 0)
			{
				char vendor[100];
				char model[100];
				char class[100];
				char module[100];
				char io[80];
				char *irq="";

	     		strcpy(vendor, extract_word(b, 1, "\t"));
       		strcpy(model, extract_word(b, 2, "\t"));
       		strcpy(module, extract_word(b, 4, "\t"));

				printf("Found device in database list : %s %s (%s)\n",vendor,model,module);

				/* Get the IO. */
				do {

					fgets(b,1024,isapnp);
				
				} while (b[0] != ' ');

				if (strstr(b,"(IO") != NULL)
				{
					char *o;


					o=strstr(b,"0x");

					for (i=0;i<strlen(o);i++)
						if (o[i] == ')')
							o[i]='\0';
					
					strcpy(io,o);
					
					printf ("Io : %s \n",io);
				}

				/* Get the IRQ. */
				do {

					fgets(b,1024,isapnp);
				
				} while (b[0] != ' ');

				if (strstr(b,"(INT") != NULL)
				{
					int i;


					irq=strstr(b,"(IRQ");

					irq[0]=irq[5];
					irq[1]='\0';
					

					printf ("IO : %s ; Irq : %s \n",io,irq);
				}

				if (strcmp(irq,io) != 0)
				{
					int retcode;
					char command[100];

					/* Try to autoload the module. */

					printf("Trying to autoconfigure the module %s.\n ",module);

					strcpy(command,"/sbin/modprobe ");
					strcat(command,module);
					strcat(command," io=");
					strcat(command,io);
					strcat(command," irq=");
					strcat(command,irq);

					retcode=system(command);

					if (retcode)
					{
						printf("Error trying to autoconfigure. %s\n",command);
						sprintf(s,"Error trying to autoconfigure: %s %s (%s)\n",vendor,model,module);
						show_msg(s);

						return 0;

					}
					else
					{
						FILE *netconf;
						FILE *mods;

						mods = fopen("/etc/modules","a");
						fprintf(mods,"%s io=%s irq=%s \n",module,io,irq);
						fclose(mods);

						netconf = fopen("/etc/cpanel/network","a+");
						fprintf(netconf,"ISA\n");
						fprintf(netconf,"%s\n",id);
						fprintf(netconf,"%s %s\n",vendor,model);
						fprintf(netconf,"%s\n",module);
						fprintf(netconf,"%s\n",io);
						fprintf(netconf,"%s\n",irq);
						fprintf(netconf,"%s\n",command);
						printf("Autoconfigured : %s %s (%s) \n",vendor,model,module);
						sprintf(s,"Autoconfigured : %s %s (%s) \n",vendor,model,module);
						show_msg(s);

						return 1;
					}
				}



				
			}
		}
		
	}

	fclose(isapnp);

	return 0;

}

int add_netcards()
{
	FILE      *kver;
	char      kernelver[20];
	DIR       *dire;
	char      *dirname[80];
	struct    dirent *kmd; /* Kernel Modules Directory. */
	int       i;
	int       idx=0;

	detect_netcards(); 
	
	kver = popen("uname -r","r");
	if (kver == NULL)
	{
		puts("I couldn't get the Kernel Version.");
		exit(0);
	}

	fgets(kernelver,20,kver);
	pclose(kver);

	kernelver[strlen(kernelver)-1]='\0'; /* Take out the ending \n */

	strcpy(dirname,"/lib/modules/");
	strcat(dirname,kernelver);
	strcat(dirname,"/net");

	//printf("Debug : %s - %s \n",kernelver,dirname);

	dire=opendir(dirname);

	if (dire == NULL)
	{
		/* I should change it to show a message box instead of this ugly
		 * message. */

		puts ("I couldn't read the net modules directory. \n");
		exit(0);
	}



	while (kmd = readdir (dire))
	{
		char modfname[80];
		FILE *f;
		char buff[1024];
		char modname[1024];
		char *moddesc;

		strcpy(modfname,kmd->d_name);

		if (modfname[0] == '.')
			continue;

		modfname[strlen(modfname)-2]='\0'; /* Take the .o out of the file name. */

		//printf ("Debug (module name) : %s \n",modfname);

		f = fopen("data/modules.desc","r");

		if (f == NULL)
		{
			printf("I couldn't open the modules description file. \n");
			exit(0);
		}

		while (!feof(f))
		{
			char *items[2][3] = {
									  {" ","1234567890","   "},
									  {" ","1234567890","   "}
	   							  };

			
			fgets(buff,1024,f);

			
			i=0;

			while (buff[i] != ':')
				i++; 

			moddesc=&buff[i+1];

			strcpy(modname,"");
			strncpy(modname,buff,i);

			modname[i]='\0';
			
			//modname=&buff[0];
			//modname[i]='\0';

			//printf("Debug : %s ; %s \n",modname,moddesc);
			
			items[0][1] = modname;
			items[0][2] = moddesc;

			if (strcmp(modname,modfname) == 0)
			{
				gtk_clist_append(GTK_CLIST(dev_clist),items[0]);
				if (find_word(modname,"/proc/modules") != NULL)
					gtk_clist_set_pixmap(GTK_CLIST(dev_clist),idx,0,pyes,yesmask);
				else
					gtk_clist_set_pixmap(GTK_CLIST(dev_clist),idx,0,pno,nomask);
				idx=idx + 1;

			}

		}

		fclose(f); 




				


		
	}

	closedir(dire);

	return 1;

}
	

gchar *selected_module="";

void install_clist_select(GtkWidget *clist, gint row, gint column,
								  GdkEventButton *event,gpointer data)
{
	gtk_clist_get_text(GTK_CLIST(clist),row,1,&selected_module);

}


void click_install_module()
{
	if (find_word(selected_module,"/proc/modules") != NULL)
		remove_module(selected_module);
	else
		install_module(selected_module);
}

void click_cancel (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy(GTK_WIDGET(data));
}

void config_dev(GtkWidget *widget, gpointer data)
{
	GtkWidget *window;
	GtkWidget *vbox1;
	GtkWidget *hbox1;
	GtkWidget *b_install;
	GtkWidget *b_cancel;
	GtkWidget *scroll_win;
	
   dev_clist = gtk_clist_new_with_titles(3,addtitle);

  	if (!add_netcards())
		return;

	
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title (GTK_WINDOW (window), "Select Network Device"); 

	gtk_widget_set_usize(GTK_WIDGET(window),500,300);

   vbox1 = gtk_vbox_new(FALSE, 0);
   gtk_widget_show(vbox1);
   gtk_container_add (GTK_CONTAINER(window),vbox1);

	scroll_win = gtk_scrolled_window_new (NULL,NULL);

	gtk_scrolled_window_set_policy (  GTK_SCROLLED_WINDOW(scroll_win),
   	  										 GTK_POLICY_AUTOMATIC,
												 GTK_POLICY_AUTOMATIC);

  	gtk_widget_show(scroll_win);

	gtk_box_pack_start(GTK_BOX(vbox1),scroll_win,TRUE,TRUE,5);



   //dev_clist = gtk_clist_new_with_titles(2,addtitle);

  	//add_netcards();
  
   gtk_widget_show(dev_clist);
	//gtk_box_pack_start(GTK_BOX(vbox1),dev_clist,TRUE,TRUE,5);
   gtk_container_add (GTK_CONTAINER(scroll_win),dev_clist);

	gtk_clist_set_column_width(GTK_CLIST(dev_clist),0,50);
	gtk_clist_set_column_width(GTK_CLIST(dev_clist),1,100);
	

	hbox1 = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox1);
	gtk_box_pack_start(GTK_BOX(vbox1),hbox1,FALSE,FALSE,5);

	b_install = gtk_button_new_with_label("Install/Remove ...");
	gtk_widget_show(b_install);
	gtk_box_pack_start(GTK_BOX(hbox1),b_install,TRUE,TRUE,5);

	b_cancel = gtk_button_new_with_label("Cancel");
	gtk_widget_show(b_cancel);
	gtk_box_pack_start(GTK_BOX(hbox1),b_cancel,TRUE,TRUE,5);

	gtk_signal_connect(GTK_OBJECT(dev_clist),"select_row",
							 GTK_SIGNAL_FUNC(install_clist_select),NULL);
	
   gtk_signal_connect(GTK_OBJECT(b_install),"clicked",
		  					GTK_SIGNAL_FUNC(click_install_module),NULL);

   gtk_signal_connect(GTK_OBJECT(b_cancel),"clicked",
		  					GTK_SIGNAL_FUNC(click_cancel),window);

	
	gtk_widget_show(window);

}
