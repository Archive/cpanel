/*  CModem
 *  Copyright (C) 1999 Andre Duarte de Souza
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>
#include "cmodem.h"
	  
int
main (int argc, char *argv[])
{
  GtkWidget *window2;
  
  gtk_set_locale ();
  gtk_init (&argc, &argv);

  window2 = create_window2 ();
  gtk_widget_show (window2);

//  init();
 Sobre_pressed(0,0);
  
  gtk_main ();
  return 0;
}
