#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void message(char *s) {
  GtkWidget *aviso;
  GtkWidget *fixed2;
  GtkWidget *button2;
  GtkWidget *label9;

  aviso = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (aviso), "aviso", aviso);
  gtk_widget_set_usize (aviso, 250, 60);
  gtk_window_set_title (GTK_WINDOW (aviso), "Warning");
  gtk_window_set_policy (GTK_WINDOW (aviso), TRUE, TRUE, FALSE);

  fixed2 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (aviso), "fixed2", fixed2);
  gtk_widget_show (fixed2);
  gtk_container_add (GTK_CONTAINER (aviso), fixed2);

  button2 = gtk_button_new_with_label ("OK");
  gtk_object_set_data (GTK_OBJECT (aviso), "button2", button2);
  gtk_widget_show (button2);
  gtk_fixed_put (GTK_FIXED (fixed2), button2, 85, 30);
  gtk_widget_set_usize (button2, 76, 25);
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (gtk_widget_destroy),
                             GTK_OBJECT (aviso));
  gtk_signal_connect_object (GTK_OBJECT (button2), "clicked",
                             GTK_SIGNAL_FUNC (gtk_main_quit),
                             NULL);
  
  label9 = gtk_label_new (s);
  gtk_object_set_data (GTK_OBJECT (aviso), "label9", label9);
  gtk_widget_show (label9);
  gtk_fixed_put (GTK_FIXED (fixed2), label9, 8, 5);
  gtk_widget_set_usize (label9, 250, 20);
  gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_CENTER);

  gtk_widget_show(aviso);
}	  

void clear(void);

/////////////// ITEM ///////////
struct item {
int number;
char *name;
char *exec;
char *icon;
char *text;
struct item *next;
};
///////////////////////////////

////////////// Head Node /////////////
struct item *head;
//////////////////////////////////////

////////////// Number of itens allocated /////
int itens = 0; 
/////////////////////////////////////////////

///////////// Alloc Next ///////
struct item *next(struct item *input) {
  struct item *temp;

  input->next = (struct item *)malloc(sizeof(struct item));
  if (!input->next) {message("Insufficient Memory");return 0;}
  temp = input->next;
  // Allocated one more item
  itens++;
  // So, give it an identity number
  temp->number = itens;
  // Just to make sure...
  temp->next = NULL;

return input->next;
}		  
///////////////////////////////

//////////////////// Free Memory ///////////////
void clear() {
struct item *x,*y;
x = head;
while (x != NULL) {
y = x->next;
free(x->name);
free(x->exec);
free(x->icon);
free(x->text);
free(x);
x = y;
}
}		  
///////////////////////////////////////////////

/////// Read cpanelrc ///////////////////////
void read_cpanelrc(struct item *temp) {
FILE *cprc;
char string[1024], ignore[2];
char byte=0;
int i=0, found=0, end=0;
char exe_end[2]=" &";

cprc = fopen("/usr/local/etc/cpanelrc","rb"); 
//cprc = fopen("cpanelrc","rb"); 
if (!cprc) {message("Couldn't find cpanelrc");return;}

while (!feof(cprc)) {
  
  end = 0;
  
  while(byte != 10 && !feof(cprc)) {
  fread(&byte,1,1,cprc);
  if (byte != 10) {string[i] = byte;i++;} else string[i] = 0; 
  } // while byte != \n

  if (!strcmp("New Item {",&string[0])) {
    found++;
  	 temp = next(temp);
			 
   while (!end){

    i=0;byte=0;
    while (byte != 32 && byte != '}') {
    fread(&byte,1,1,cprc);
    if (byte != 32 && byte != '}') {string[i] = byte;i++;}
    } // while byte != " "
	 string[i] = 0;
    
	 if (byte != '}') fread(&ignore[0],2,1,cprc); 
	 else {fread(&ignore[0],1,1,cprc);end=1;}
	 
    if (!strcmp("name",&string[0])) {
	 byte = 0; i = 0; 
    while (byte != ';') {
	 fread(&byte,1,1,cprc);
    if (byte != ';') {string[i] = byte; i++;}
	 } // while
	 string[i] = 0;
	 temp->name = (char *)malloc(sizeof(string));
	 strcpy(temp->name,&string[0]);
    fread(&ignore[0],1,1,cprc);
	 }	// if name	

	 if (!strcmp("exec",&string[0])) {
	 byte = 0; i = 0; 
    while (byte != ';') {
	 fread(&byte,1,1,cprc);
    if (byte != ';') { string[i] = byte; i++;}
	 } // while
	 string[i] = 0;
	 temp->exec = (char *)malloc(sizeof(string)+2);
	 strcpy(temp->exec,&string[0]);
    strcat(temp->exec,exe_end);
	 fread(&ignore[0],1,1,cprc);
	 }	// if exec 

	 if (!strcmp("icon",&string[0])) {
	 byte = 0; i = 0; 
    while (byte != ';') {
	 fread(&byte,1,1,cprc);
    if (byte != ';') { string[i] = byte; i++;}
	 } // while
	 string[i] = 0;
	 temp->icon = (char *)malloc(sizeof(string));
	 strcpy(temp->icon,&string[0]);
    fread(&ignore[0],1,1,cprc);
	 }	// if icon

	 if (!strcmp("text",&string[0])) {
	 byte = 0; i = 0; 
    while (byte != ';') {
	 fread(&byte,1,1,cprc);
    if (byte != ';') { string[i] = byte; i++;}
	 } // while
	 string[i] = 0;
	 temp->text = (char *)malloc(sizeof(string));
	 strcpy(temp->text,&string[0]);
    fread(&ignore[0],1,1,cprc);
	 }	// if text

	} // while !end 	 
  } // if New Item
 
} // while !feof
fclose(cprc);
if (!found) {message("No Itens Found");return;} // clear();exit(1);}
}
/////////////////////////////////////////////

void init_plugin() {
head = (struct item *)malloc(sizeof(struct item));
if (!head) {message("Insufficient Memory");return;}
head->number = 0;
read_cpanelrc(head);
}
