/*  CModem
 *  Copyright (C) 1999 Andre Duarte de Souza
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

GtkWidget *text1;
GtkWidget *b1,*b2,*b3,*b4;
char achou=0;
char *dev;
//
//#include "modem.h"

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

//extern GtkWidget *text1;

int modem,i;
char first='M' & 31; //ent = 10
struct termios back;

void tiraok(char *s,int size) {
int i;
for (i=0;i<size;i++)
 if (s[i] == 10 && s[i+1] == 'O' && s[i+2] == 'K') s[i] = 0;
}	  

void diz(char *s) {
gtk_text_insert(GTK_TEXT(text1),NULL,NULL,NULL,s,-1);	  
}	  

void limpa() {
int i;
gchar *y;
y = gtk_editable_get_chars(GTK_EDITABLE(text1),0,-1); 
i=strlen(y);
gtk_text_set_point(GTK_TEXT(text1),0);
gtk_text_forward_delete(GTK_TEXT(text1),i);
gtk_text_set_point(GTK_TEXT(text1),0);
g_free(y);
}	  

// Manda 's' pro modem
void send(char *s,char quanto) {
//sleep(1);
write(modem,&first,1);
//sleep(1);
for (i=0;i<quanto;i++) write(modem,&s[i],1);
write(modem,&first,1);
sleep(1);
}	  

// Tira o ultimo comando enviado e coloca a resposta do modem
// em 'recebi'.
char checa(char quanto) {
char recebi[127];
read(modem,&recebi[0],quanto);
read(modem,&recebi[0],127);
//printf("\n\nString : %s\n",recebi);
for (i=0; i<127; i++) 
  if (recebi[i] == 'O' && recebi[i+1] == 'K') return 1; 
return 0;
}

void pega_string() {
int k=0;
char recebi[127];
send("ati3",4);
read(modem,&recebi[0],5);
read(modem,&recebi[0],127);	  
tiraok(recebi,127);
while (recebi[k] != 10) k++;
k++;
diz("\nModem : ");
diz(&recebi[k]);
}	  

void p_init() {
struct termios tty;
int mcs;

tcgetattr(modem,&tty);
tcgetattr(modem,&back);
cfsetospeed(&tty, B57600);
cfsetispeed(&tty, B57600);
tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;

tty.c_iflag = IGNBRK;
tty.c_lflag = 0;
tty.c_oflag = 0;
tty.c_cflag |= CLOCAL | CREAD;

tty.c_cc[4] = 1; //VMIN
tty.c_cc[5] = 5; //VTIME

tty.c_cflag &= ~(PARENB | PARODD);

tcsetattr(modem,TCSANOW, &tty);

ioctl(modem, TIOCMGET, &mcs);
mcs |= TIOCM_RTS;
ioctl(modem, TIOCMSET, &mcs);

}	  

void p_back() {
tcsetattr(modem, TCSANOW, &back);	  
}	  


void Detectar_pressed(GtkButton *x, gpointer y) {
int z;
char device[11] = "/dev/ttyS0"; //devices a serem analisados

if (!achou) {

//diz("\n");
limpa();
diz("Analizing...\n");

for (z=0; z<4; z++) {    // devices ttySx onde x vai de 0 a 3 (i=0;i<4)
device[9] = z + '0';     // o z + '0' transforma int na tabela ascii (0 a 3)
//diz("%s - ",device);  // imprime o device analisado no momento
diz(device);
diz(" - ");
modem=open(device,O_RDWR|O_NDELAY); // read n write & nao em bloco
if (modem) {
p_init();
sleep(1);
flock(modem,LOCK_EX);    // ativa lockfile
send("atz",3);           // manda atz pro modem (manda 3 caracteres (a,t e z))
if (checa(4)) {diz("Modem found!\n");pega_string();achou=1;break;} 
else {diz("Modem not found\n");p_back();}
flock(modem,LOCK_UN);    // desativa lockfile
close(modem);
} else diz("Permisson denied\n");
}
if (achou) {device[10]=0;if (symlink(device,"/dev/modem")) {limpa();diz("Error :\nI couldn't create the link\n");}}

diz("\n\n");
//return 0;
}
}


//
//
void Sair_pressed(GtkButton *x, gpointer y) {
gtk_main_quit();	  
}	  

void Refazer_pressed(GtkButton *x, gpointer y) {
limpa();
if (remove("/dev/modem")) diz("Error :\nCouldn't remove /dev/modem");
else diz("Link /dev/modem removed.\n\nClick in Detect or Select...");
}	  


void init() {
  char recebi[2048];
  
  achou = 0;
  
  modem = open("/dev/modem",O_RDWR|O_NDELAY);
  if (modem>=0) {achou=1;close(modem);}
 
  if (achou) {
  limpa();
  diz("Modem already configured : \n");
  modem=open("/dev/modem",O_RDWR|O_NDELAY);
  p_init();
  send("atz",3);
//  read(modem,&recebi[0],127);
  if (!checa(4)) {limpa();diz("I couldn't find your modem \nin that port");p_back();return;}
  pega_string(); 

  send("ati7",4);
  read(modem,&recebi[0],5);
  read(modem,&recebi[0],2048);

  close(modem);  
  tiraok(recebi,2048);
  
  diz(recebi);
  } else Detectar_pressed(NULL,NULL);
}

void veai(GtkWidget *x, gpointer data) {
if (data == GTK_OBJECT(b1)) dev = "/dev/ttyS0";
if (data == GTK_OBJECT(b2)) dev = "/dev/ttyS1";
if (data == GTK_OBJECT(b3)) dev = "/dev/ttyS2";
if (data == GTK_OBJECT(b4)) dev = "/dev/ttyS3";
//printf("\n%s\n",dev);
}	  

void cria_achou(GtkWidget *x, gpointer y) {
diz(dev);
if (symlink(dev,"/dev/modem")) {limpa();diz("Error :\nCouldn't create link");
}
init();
}	  

// GIGANTE
void Selecionar_pressed(GtkButton *x, gpointer y) {
  GtkWidget *window3,*button,*separator;

  GtkWidget *box1,*box2;
  GSList *group;
  
  window3 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window3), "Select", window3);
  gtk_widget_set_usize (window3, 110, 180);
  gtk_window_set_title (GTK_WINDOW (window3), "Port");
  gtk_window_set_policy (GTK_WINDOW (window3), TRUE, TRUE, FALSE);	  
  
  box1 = gtk_vbox_new(FALSE,0);
  gtk_container_add(GTK_CONTAINER(window3), box1);
  gtk_widget_show(box1);

  box2 = gtk_vbox_new(FALSE,10);
  gtk_container_border_width(GTK_CONTAINER(box2),10);
  gtk_box_pack_start(GTK_BOX(box1), box2, TRUE, TRUE, 0);
  gtk_widget_show(box2);
  
  b1 = gtk_radio_button_new_with_label (NULL, "/dev/ttyS0");
  gtk_box_pack_start(GTK_BOX(box2), b1, TRUE, TRUE, 0);
  gtk_widget_show(b1);
  group = gtk_radio_button_group(GTK_RADIO_BUTTON(b1));
  dev = "/dev/ttyS0";
  
  b2 = gtk_radio_button_new_with_label (group, "/dev/ttys1");
  gtk_box_pack_start(GTK_BOX(box2), b2, TRUE, TRUE, 0);
  gtk_widget_show(b2); 
  group = gtk_radio_button_group(GTK_RADIO_BUTTON(b2));
  
 b3 = gtk_radio_button_new_with_label (group, "/dev/ttyS2");
  gtk_box_pack_start(GTK_BOX(box2), b3, TRUE, TRUE, 0);
  gtk_widget_show(b3);  
  group =gtk_radio_button_group(GTK_RADIO_BUTTON(b3));

 b4 = gtk_radio_button_new_with_label (group, "/dev/ttyS3");
  gtk_box_pack_start(GTK_BOX(box2), b4, TRUE, TRUE, 0);
  gtk_widget_show(b4);  
  group = gtk_radio_button_group(GTK_RADIO_BUTTON(b4)); 
  
  
  separator = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(box1), separator, FALSE, TRUE, 0);
  gtk_widget_show(separator);
  
  gtk_signal_connect_object(GTK_OBJECT(b1), "toggled", GTK_SIGNAL_FUNC(veai), GTK_OBJECT(b1));
  gtk_signal_connect_object(GTK_OBJECT(b2), "toggled", GTK_SIGNAL_FUNC(veai), GTK_OBJECT(b2));
  gtk_signal_connect_object(GTK_OBJECT(b3), "toggled", GTK_SIGNAL_FUNC(veai), GTK_OBJECT(b3));
  gtk_signal_connect_object(GTK_OBJECT(b4), "toggled", GTK_SIGNAL_FUNC(veai), GTK_OBJECT(b4)); 
  
  box2 = gtk_vbox_new(FALSE, 10);
  gtk_container_border_width(GTK_CONTAINER(box2),10);
  gtk_box_pack_start(GTK_BOX(box1),box2,FALSE,TRUE,0);
  gtk_widget_show(box2);
  
  button = gtk_button_new_with_label("OK");
  gtk_signal_connect_object(GTK_OBJECT(button), "released", GTK_SIGNAL_FUNC(cria_achou), GTK_OBJECT(window3));
  gtk_signal_connect_object(GTK_OBJECT(button), "released", GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(window3));
  
  
  gtk_box_pack_start(GTK_BOX(box2), button, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(button);
  gtk_widget_show(button);
  gtk_widget_show(window3);
    
}	  


void Sobre_pressed(GtkButton *x, gpointer y) {
limpa();
diz("Modem detector\n");	  
diz("Copyleft @ 1999 Andr� Souza\n\n");
diz("This program belongs to \n");
diz("The Configurator Panel.\n\n");
diz("The Configurator Panel :\nCopyright @ 1999 LinuxLabs.\n\n");
}	  

GtkWidget*
get_widget                             (GtkWidget       *widget,
                                        gchar           *widget_name)
{
  GtkWidget *found_widget;

  if (widget->parent)
    widget = gtk_widget_get_toplevel (widget);
  found_widget = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (widget),
                                                   widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}

/* This is an internally used function to set notebook tab widgets. */
void
set_notebook_tab                       (GtkWidget       *notebook,
                                        gint             page_num,
                                        GtkWidget       *widget)
{
  GtkNotebookPage *page;
  GtkWidget *notebook_page;

  page = (GtkNotebookPage*) g_list_nth (GTK_NOTEBOOK (notebook)->children, page_num)->data;
  notebook_page = page->child;
  gtk_widget_ref (notebook_page);
  gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), page_num);
  gtk_notebook_insert_page (GTK_NOTEBOOK (notebook), notebook_page,
                            widget, page_num);
  gtk_widget_unref (notebook_page);
}

GtkWidget*
create_window2 ()
{
  GtkWidget *window2;
  GtkWidget *fixed1;
  GtkWidget *vbuttonbox1;
  GtkWidget *Selecionar;
  GtkWidget *Sobre;
  GtkWidget *Detectar;
  GtkWidget *Sair;
  GtkWidget *Refazer;

  window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (window2), "window2", window2);
  gtk_widget_set_usize (window2, 400, 300);
  gtk_window_set_title (GTK_WINDOW (window2), "Modem");
  gtk_window_set_policy (GTK_WINDOW (window2), TRUE, TRUE, FALSE);
  gtk_signal_connect (GTK_OBJECT (window2), "destroy",
                      GTK_SIGNAL_FUNC (gtk_main_quit),
                      NULL);
 
  fixed1 = gtk_fixed_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "fixed1", fixed1);
  gtk_widget_show (fixed1);
  gtk_container_add (GTK_CONTAINER (window2), fixed1);

  vbuttonbox1 = gtk_vbutton_box_new ();
  gtk_object_set_data (GTK_OBJECT (window2), "vbuttonbox1", vbuttonbox1);
  gtk_widget_show (vbuttonbox1);
  gtk_fixed_put (GTK_FIXED (fixed1), vbuttonbox1, 8, 8);
  gtk_widget_set_usize (vbuttonbox1, 95, 145);

  Detectar = gtk_button_new_with_label ("Detect");
  gtk_object_set_data (GTK_OBJECT (window2), "Detectar", Detectar);
  gtk_widget_show (Detectar);
  gtk_container_add (GTK_CONTAINER (vbuttonbox1), Detectar);
  gtk_widget_set_usize (Detectar, 77, 29);
  gtk_signal_connect (GTK_OBJECT (Detectar), "released",
                      GTK_SIGNAL_FUNC (init),
                      NULL);

  Selecionar = gtk_button_new_with_label ("Select");
  gtk_object_set_data (GTK_OBJECT (window2), "Selecionar", Selecionar);
  gtk_widget_show (Selecionar);
  gtk_container_add (GTK_CONTAINER (vbuttonbox1), Selecionar);
  gtk_widget_set_usize (Selecionar, 77, 29);
  gtk_signal_connect (GTK_OBJECT (Selecionar), "released",
                      GTK_SIGNAL_FUNC (Selecionar_pressed),
                      NULL);

  Refazer = gtk_button_new_with_label ("Undo");
  gtk_object_set_data (GTK_OBJECT (window2), "Refazer", Refazer);
  gtk_widget_show (Refazer);
  gtk_container_add (GTK_CONTAINER (vbuttonbox1), Refazer);
  gtk_widget_set_usize (Refazer, 77, 29);
  gtk_signal_connect (GTK_OBJECT (Refazer), "released",
                      GTK_SIGNAL_FUNC (Refazer_pressed),
                      NULL);

  Sobre = gtk_button_new_with_label ("About");
  gtk_object_set_data (GTK_OBJECT (window2), "Sobre", Sobre);
  gtk_widget_show (Sobre);
  gtk_container_add (GTK_CONTAINER (vbuttonbox1), Sobre);
  gtk_widget_set_usize (Sobre, 77, 29);
  gtk_signal_connect (GTK_OBJECT (Sobre), "released",
                      GTK_SIGNAL_FUNC (Sobre_pressed),
                      NULL);

  Sair = gtk_button_new_with_label ("Exit");
  gtk_object_set_data (GTK_OBJECT (window2), "Sair", Sair);
  gtk_widget_show (Sair);
  gtk_container_add (GTK_CONTAINER (vbuttonbox1), Sair);
  gtk_widget_set_usize (Sair, 77, 29);
  gtk_signal_connect (GTK_OBJECT (Sair), "released",
                      GTK_SIGNAL_FUNC (Sair_pressed),
                      NULL);

  text1 = gtk_text_new (NULL, NULL);
  gtk_object_set_data (GTK_OBJECT (window2), "text1", text1);
  gtk_widget_show (text1);
  gtk_fixed_put (GTK_FIXED (fixed1), text1, 112, 8);
  gtk_widget_set_usize (text1, 278, 282);

  return window2;
}


